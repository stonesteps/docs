# 🚦Stone Steps - Work in Progress 🚦

This project is very much a Work in Progress. 

Most of the work is being done on private servers made for a large enterprise application, so that cannot be 
shared. The strategies used, however, can be shared; which is what this is.

[Discord Community Server](https://discord.gg/Fhr6frA2)

## Overview

Stone Steps is more of a "way to work" than it is a framework, with the goal to be as un-opinionated as possible while 
working with the tools provided to us by the web browser itself. The browser is very efficient, so to take advantage of
its efficiencies, we need to develop in the way that works best with the browsers, including any "opinions" they have.

The web has been around for a long time now, and a lot of the existing problems have solutions at the JS framework 
level. Today, however, this is no longer required as the problems have largely been solved directly in things like the 
browser itself, EcmaScript updates, HTML and CSS updates as well as even the Node ecosystem itself.

## Why the name "Stone Steps"?

The name Stone Steps comes from the main technologies that are being used here;

`Stone` is for using [SolidJS](https://www.solidjs.com/), since stone is quite "solid". SolidJS is very powerful and
has done a lot to advance the JavaScript community. Well done! 👏

`Steps` is for using [Vite](https://vitejs.dev/), since Vite is the French word for "Quick", "Steps" are made to help us
to move quickly up or down.
Vite 2.0 has helped the bundling out a lot, allowing us to use HTML entry points and create ESM builds targeting the
browser, NodeJS or a code library.

## Who are you?

My name is [Steven Stark](https://stevenstark.com/), and I have been making websites and multimedia since 1995. I've
been doing this work for so long that I feel that I have a bit of a unique insight into the JavaScript world.

When I started developing, there were no JavaScript frameworks. Everything had to be made from scratch.

Today, it feels like there's a new JavaScript framework built for every little problem that pops up. We've gotten used 
to relying on frameworks to solve problems that we were always expected to solve ourselves. The frameworks are simply 
doing "too much", and it is time to step away from them.

### What this is?

This is a way to work with web components in *ANY* stack that supports including an HTML page.

The main use here is for rendering JSON data in a JSX template, be that in a WebComponent or larger framework. The JSX
templates should be updated reactively by taking advantage of Signals.

SolidJS is used for the Signals and JSX templates, which is important because SolidJS has the same un-opinionated 
approach to development.

Vite is used for the build tool, which is important for web component delivery via multiple entry points. With multiple 
entry points, we can group common web components together, making them portable. This is done with ECMAScript Modules
( ESM ), and some manual organization from the developer. 

The goal here is to require as little as possible. This uses ESM, but can include polyfills to provide support
for legacy browsers. This uses SolidJS ( via [solid-element](https://github.com/solidjs/solid/tree/main/packages/solid-element#readme) ) 
to develop Web Components, which can be used on their own or as part of any larger framework. There is very little here 
that requires SolisJS to even be used, but SolidJS has solved the problems that we may otherwise need to solve. What we 
really want are JSX templates with reactive types known as Signals.

Note: Signals are anticipated to become part of JavaScript itself, hopefully via [tc39](https://github.com/tc39/proposal-signals)

This project is for use at build time, not at run time. We are building a new version of components to be installed into 
a website or product via JS, HTML or a template engine such as Thymeleaf. This allows us to create Release Candidates 
that can be properly tested.

This is made primarily for website development.

I strongly believe that there is no longer a need for all of these JavaScript frameworks to be solving things for us.
Instead, I believe that developers should be empowered to create their own solutions.

We have the tools now. The open source community has been amazing at separating the libraries from the frameworks. We 
don't need to use [Angular](https://angular.dev/) to use [Axios](https://axios-http.com/docs/intro), and we don't need 
to use [React](https://react.dev/) to use [JSX](https://facebook.github.io/jsx/).

**REQUIRED READING:**: We use SolidJS, which will require a developer to understand how Signals and JSX works. Please
ensure that you are familiar with their excelent [documentation](https://www.solidjs.com/docs/latest). Just keep in mind 
that we are not using the full framework, so some things do not apply. see: [Solid Element](https://github.com/solidjs/solid/tree/main/packages/solid-element#readme)

These tools are made for build time in order to deliver JavaScript, with it's associated CSS and other static assets,
via either HTML or JS. Using HTML can be very useful for systems that use HTML template engines, such at [Thymeleaf](https://www.thymeleaf.org/).
Since Vite supports HTML entry points, Vite also supports Thymeleaf by default since Thymeleaf is semantically 
compatible with HTML.

### What this isn't?

This is not a full framework, and does not include anything to do with server logic. 

There is no router. 

There are no microservices.

There is no requirement for NodeJS to be part of the server stack.

This is not intended for "App" development. The approaches taken here expect full page reloads to happen on almost all
Hyperlinks. It is not expected to be preventing a page to reload. Because of this, it is extra important to support
browser caching.

This is not made for live dynamic updating, if you need a live CMS then that will need to be implemented separately.

### Wait, no router? 

Yes, no router. Who told framework developers that they needed to develop JS routers or to do the job of the server or 
web browser? Every attempt at this has caused problems since the beginning, and any solution that comes with its own 
router is an issue for many back end servers. This is especially true for any legacy systems built with a Server Side 
Rendered ( SSR ) using a Model View Controller ( MVC ) architecture, which is very common.

This comes with some opinions of their own:

* URLs should be bookmarkable and shareable.
* Search Engine Optimization works better with bookmarkable URLs
* Hyperlinks should reload the page.
* static assets should be cached by the browser by having unique filenames. 
* no "cache-busting" assets by adding `?something=random` to the url

Yes, these are "opinions" and we are wanting to avoid those, however these are "opinions" that have already been made 
and imposed on us by the web browser ( and google ), so it is better to work *WITH* these opinions than trying to work
*AGAINST* them by requiring some, other, solution made just for JavaScript.

#### Other frameworks to check out:

if you are wanting a full framework, I would suggest looking at one of the following:

* [SolidStart](https://start.solidjs.com/) If you do want server integration, I would highly suggest starting here.
* [Nuxt](https://nuxt.com/) Vue is great, if you use Vue this is the framework for you.
* [Nitro](https://nitro.unjs.io/) I haven't had the time to use this, but I've heard great things
* [React 19](https://react.dev/blog/2024/04/25/react-19#whats-new-in-react-19) I have moved far away from React ( and 
Angular ) due to the frameworks solving problems that largely don't exist anymore. That said, React 19 is moving in the
right direction. If you're already using React, take a look at this first.
* [Next.js](https://nextjs.org/) Next.js is taking the opposite approach from this project in that they want to solve
every problem for you and try to make it easier to jump in and code. This is great for some, but is far too opinionated 
for others like myself.


### Web Components? Why? Are they needed?

Web Components have been debated, and version 1 was not very well received. Version 2 is has been around for some time 
now, and solves many of the issues, however there are still issues around shared CSS, when to use their new Shadow DOM, 
and how to work with multiple instance of a web component on a page at once. 

They take a bit of a mental shift.

This project will try to show how to work with Web Components, but at the end of the day all of this is completely 
optional. This all works with regular 'ol HTML, but I will be focusing on Web Components.

I use them in order to better support an incremental release from an existing, legacy, front end to a new front end. The 
Web Components allow me to "mix and match" the old with the new in that I can leave the old HTML alone and only replace 
one section at a time with a new Web Component version. This approach allows us to have incremental releases, which is 
preferred over any kind of "big bang" release. Eventually, everything will be migrated into Web Components, allowing
changes to be made quickly with modern tools.

No, there is nothing here that strictly requires Web Components, I just happen to like them. I hope to show versions
of this without using web components at all, but this will need to come later.

### ANY Stack, really?

Yes, *ANY* stack. This is because there is no requirement for any server side stack at all. The output of this project
is JavaScript, HTML, CSS and possibly some static assets. This output can be bundled for any server to render, or it
could be served statically with only a file server. It makes no difference, and I encourage developers to use both 
approaches.

### What about Static sites?

Static sites are perfect for this! During development, it is expected that we work with static demo pages and mock data,
which means that static site development is at the core of this project. 

The output is very static and portable, but can still integrate into existing systems.

### State Management

State management is one of the oldest problems to be solved when it comes to website, however they are typically quite
simple problems to actually solve. It can often be the case that state management is assumed to be a much larger issue
than it actually is. Most websites have their state ready on page load and will rarely need to update the state without
a new page load.

#### One - to - Many

Stone Steps uses a one-to-many approach related to the web component instances on a given page. There can only be one
instance of any given data provider, however there can be many instances of each JSX web component. 

Note: There is no enforcement of this single instance rule in order to allow for a manager or factory class to be added, 
which could then support multiple instances of a given data provider. Without a manager or factory, it is expected that
the developer will know to follow this rule.

There are three main types of Components:

1) **Data Provider Web Components**. 
  * There is only allowed to be a single instance of a data provider component on any given page. 
  * Each data provider is responsible for its own domain
  * No data provided will mutate the data of another provider. 
  * All data updates go through this, single, data provider instance. 
  * The Data Provider holds the [Signals](https://www.solidjs.com/tutorial/introduction_signals) that the view 
  components will reference in order to update their views.

2) **JSX Web Components**. 
  * There can be many instances of the view components
  * The size is expected to be fairly large portions of the site at once. A handful of larger JSX web components are 
    preferred over many instances of small JSX web components.
  * All view component instances reference the same, single, instance of a data provider.
  * View components render JSX templates that reference the Signals from the data provider.
   
3) **Function Components**.
  * These are only available in the JS source directly, and are not exposed as web components to the browser.
  * Fairly standard way to externalize and share JSX code between components.
  * Sometimes we run into cases where something won't work in just a functional component, but does in a web 
    component. example: UnoCSSPlaceholder only works in web components and not functional components.

NOTE:

This concept is not represented by the HTML; it is only in the JavaScript runtime itself.

This data provider to web component relationship is not represented by the HTML, so there is no requirement for the Web
Component Node to be a child of the associated Data Provider node, as is the case in frameworks like React. This
relationship is established by the JS code itself when importing the data provider in the web component class.

To help illustrate this idea, I made a diagram to show the relationships between two data provider components and three 
view components.

```pre
┌──────────────┐       ┌──────────────┐
│   User Data  │       │  Site Data   │
│   Provider   │       │  Provider    │
└─┬────────────┘       └────────┬─────┘
  │     ┌──────────┬──────────┬─┘      
  │     │          │          │        
┌─▼─────▼──┐  ┌────▼─────┐  ┌─▼───────┐
│ Header   │  │ Main Body│  │ Footer  │
│ Comp.    │  │ Comp.    │  │ Comp.   │
└──────────┘  └──────────┘  └─────────┘ 
```

#### Data default state

We include the default state in the web components themselves, but often this may mean that nothing is shown until the
initial state has been updated.

#### Data Updating initial state

State data is largely available at the render time, so it can be provided in the rendered HTML. This avoids the need for 
an API, and instead mimics the same rendering approach that would be done for showing the same data in HTML.

The web components then request the JSON from the page, using a known JS namespace. To clean things up, the JSON from 
the HTML can be deleted so that it is not shown when viewing the page source, but this step is optional.

#### Data Updating state changes

Typically, there are not a lot of states that need updating without a full page load. Because of the low number of these 
use cases, we don't always need to build an API. Instead, we can use the server template engine ( ex: Thymeleaf )
templates to provide the rendered JSON for updating the state changes as well.

The web components request the page like any other, and pull the JSON from the response body. This is emulating an API.

## JAVA REAL WORLD EXAMPLE

### Do you have an example? This would help to understand what the point is and how this helps.

Yes! This approach is being used in my day job on enterprise products, which uses Java on the server.

On this project, there are dozens of Java services, most of them running legacy code bases. This process was developed 
in order to update the legacy front end with a modern one, but without changing too much at once. No "big bang" 
releases, instead we roll our incremental changes with a hybrid approach. The "old" and the "new" can be on the same
page, acting as good neighbours.

In order to assist with this process, we opted to convert our Java view renderer from Java Service Page's (JSP's), to a 
HTML based renderer called Thymeleaf. ( Thymeleaf was already desired, so it was a prefect fit for us. )

The Stone Steps process will produce a UI library that is bundled into WebJars, that are installed into the Maven 
registry. The Java service will include the WebJar as a dependency un the pom.xml file, and include it in the next 
build. Both the Java service, and the WebJar, are using Thymeleaf as the rendering engine. By using Thymeleaf, we have 
support for HTML since Thymeleaf respects the HTML spec. This allows us to build an index.html template from vite, which 
is loaded in the main thymeleaf template. This index.html template contains all the built JS and other assets from our 
project.

There is also a companion tool that can preview the thymeleaf templates using mock data so that we can be sure how they
will look after final integration. This greatly speeds up development, and allows for quick QA and demos before 
integration happens.

By using this process, we can take advantage of the right tools for the job at the right place and time.

### That sounds like a lot is happening. Is this all happening in the same code base?

Yes, there is a lot going on in that above example. And, no, this is not all in the same code base; there are a few git
repositories that are working together in order to deliver a new Java Release Candidate:

#### Common UI repo ( and other web component repos )

We currently only have one of these repos, but there could be as many as you'd like.

This repo is an NPM project that publishes to a NPM registry

The output of this repo includes:

* a suite of Web Components with any associated assets.
* message bundles for the Java Service
* .json version of the message bundles to be used as default values in the web components
* .css files for the web components ( we use DaisyUI on this project, but we can just assume tailwind here )
* Vite builds for library mode with web components organized into multiple entry points

#### Frontend Webjar repos

We currently have many of these repos, one for each Java service.

This repo is a NPM project that publishes to a Maven registry.

The only NPM dependencies are the UI libraries that contain versioned web components such as the Common UI

The output of this repo includes:

* All the Thymeleaf templates for the Java Service
* A built index.html Thymeleaf template that includes the web components from the Common UI
* message bundles for the Java Service
* Vite builds for production by generating a html entrypoint from the provided web components.

#### Java Service

This repo in a Maven project that creates a Java Service

That output of this repo remains the same, for us that means either a RPM to be installed by Chef or a version of the 
Docker image is created and pushed into the docker registry for future use.

At this point, the Java service is going to be opinionated about where and how the new release candidate is to be 
tested and then released, and will follow their existing processes to do so.

The server stack is left unchanged, leaving our routing alone. The routing is advanced on this project and is a mix
of Java service routing and other routing at the ingress and load balancer. Forcing these to use routing defined by the 
front end code base is not desired, and is a primary deciding factor in why other frameworks should not be used.


